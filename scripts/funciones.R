corfun <- function(probe) {
  #Indice de sonda correspondiente en el arreglo (features Hard coded)
  array.index <- which(features==probe)
  #su gene_id correspondiente  (geneids Hard coded)
  geneId <- geneids[array.index]
  #Obtener targets
  targets<-mget(geneId, targetscan.Hs.egTARGETS)[[1]]
  targets<-unique(targets)
  #Pasar a base, regresa de todas las especies.
  target.miRNAS <- mget(targets, targetscan.Hs.egFAMILY2MIRBASE )
  # Recuperar solo aquellos Homo Sapiens.
  target.miRNAS <- sapply(target.miRNAS, function(x) grep("hsa",x,value=TRUE))
  target.miRNAS <- unlist(target.miRNAS)
  # Cuales miRNAS cuantificados se encuentran entre estos targets
  miRdex <- which(mirData$miRNA %in% target.miRNAS)
  # Calcular correlacion entre expresion de mirna y su target para cada uno
  cors<-sapply(miRdex, function(y) cor(exprs(commonSet)[array.index,] , as.numeric(mirData[ y,2:11]) ) )  
  names(cors)<-mirData$miRNA[miRdex]
  return(cors)
}

graficaMvE<-function(probeName, miRname){
  which(featureNames(commonSet)==probeName)->probeInd
  which(mirData$miRNA==miRname)->mirInd
  plot(exprs(commonSet)[probeInd,], mirData[mirInd,2:11], xlab="RNA-Expression", ylab="miR-Expression")  
}

getSymbol <- function(probeName) {
  which(geneTranscript==probeName)->foo
  return(geneSymbol[foo])
}

#mData harcoded
getInd <- function( mirname ) which(colnames(mData) == mirname)

getCor<-function( mirnaCor ) {  
  ind<-sapply(names(mirnaCor), getInd)
  data.frame( ind=ind, cor=mirnaCor )
}
